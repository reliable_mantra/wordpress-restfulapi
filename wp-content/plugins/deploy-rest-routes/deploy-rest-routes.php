<?php
/*
   Plugin Name: Deploy Rest Routes
   Version: 1.0
   Author: Ivan Stantic
   Description: Custom Restful API Routes
   Text Domain: rest-routes
   License: GPLv3
  */

$RestRoutes_minimalRequiredPhpVersion = '7.0';

/**
 * Check the PHP version and give a useful error message if the user's version is less than the required version
 * @return boolean true if version check passed. If false, triggers an error which WP will handle, by displaying
 * an error message on the Admin page
 */
function RestRoutes_noticePhpVersionWrong() {
	global $RestRoutes_minimalRequiredPhpVersion;
	echo '<div class="updated fade">' .
	     __('Error: plugin "Rest Routes" requires a newer version of PHP to be running.',  'rest-routes').
	     '<br/>' . __('Minimal version of PHP required: ', 'rest-routes') . '<strong>' . $RestRoutes_minimalRequiredPhpVersion . '</strong>' .
	     '<br/>' . __('Your server\'s PHP version: ', 'rest-routes') . '<strong>' . phpversion() . '</strong>' .
	     '</div>';
}


function RestRoutes_PhpVersionCheck() {
	global $RestRoutes_minimalRequiredPhpVersion;
	if (version_compare(phpversion(), $RestRoutes_minimalRequiredPhpVersion) < 0) {
		add_action('admin_notices', 'RestRoutes_noticePhpVersionWrong');
		return false;
	}
	return true;
}

/**
 * Custom Restful API Routes
 */
add_action('rest_api_init', 'deploy_rest_route');

function deploy_rest_route()
{
	register_rest_route('route/v1', 'deployed', [
		'methods' => WP_REST_SERVER::READABLE, // 'GET'
		'callback' => 'deployed_rest_route_callback',
	]);
}

function deployed_rest_route_callback()
{
	$query = new WP_QUERY([
		'post_type' => 'post',
	]);

	$response = [];

	while ($query->have_posts()) {
		$query->the_post();

		$response[] = [
			'title' => get_the_title(),
			'permalink' => get_the_permalink(),
		];
	}

	return $response;
}
